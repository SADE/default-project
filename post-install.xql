xquery version "3.1";

declare variable $target external;

(
    xmldb:move($target||"/sade-projects", "/db"),
    xmldb:move($target||"/system/config/db/sade-projects", "/db/system/config/db"),
    (: xmldb:move($target||"/system/autostart", "/db/system") :)
    xmldb:reindex("/db/sade-projects/textgrid/data/xml/data"),
    (: set correct mode for autostart trigger :)
    sm:passwd("admin", "REPLACE_ME")
)
